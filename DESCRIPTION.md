## About

XBackBone is a simple, self-hosted, lightweight PHP file manager that support the instant sharing tool ShareX and \*NIX systems. It supports uploading and displaying images, GIF, video, code, formatted text, and file downloading and uploading. Also have a web UI with multi user management, past uploads history and search support.

## Main Features

* Supports every upload type from ShareX.
* Config generator for ShareX.
* Low memory footprint.
* Multiple backends support: Local storage, AWS S3, Google Cloud, Azure Blob Storage, Dropbox, FTP(s).
* Web file upload.
* Code uploads syntax highlighting.
* Video and audio uploads webplayer.
* PDF viewer.
* Files preview page.
* Bootswatch themes support.
* Responsive theme for mobile use.
* Multi language support.
* User management, multi user features, roles and disk quota.
* Public and private uploads.
* Logging system.
* Share to Telegram.
* Linux supported via a per-user custom generated script (server and desktop).
* Direct downloads using curl or wget commands.
* Direct images links support on Discord, Telegram, Facebook, etc.
* System updates without FTP or CLI.
* Easy web installer.
* LDAP authentication.
* Registration system.
* Automatic uploads tagging system.
