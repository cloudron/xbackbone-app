#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(username, password) {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[text()="Login"]')).click();
        await waitForElement(By.id('userDropdown'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/logout');
        await waitForElement(By.id('username'));
    }

    const headContent = '<meta name="keywords" content="xbackbone on cloudron!"></meta>';

    async function setHeadContent() {
        await browser.get('https://' + app.fqdn + '/system');
        await waitForElement(By.xpath('//textarea[@name="custom_head"]'));
        await browser.findElement(By.xpath('//textarea[@name="custom_head"]')).sendKeys(headContent);
        await browser.findElement(By.xpath('//form[contains(@action, "/save")]/button[@type="submit"]')).click();
        await waitForElement(By.xpath('//div[contains(text(), "System settings saved!")]'));
    }

    async function checkHeadContent() {
        const response = await superagent.get('https://' + app.fqdn);
        if (!response.text.includes(headContent)) throw new Error(`Could not find head content: ${response.text}`);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can set head content', setHeadContent);
    it('can check head content', checkHeadContent);
    it('can logout', logout);

    it('can ldap login', login.bind(null, username, password));
    it('can logout', logout);

    it('restart app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can check head content', checkHeadContent);
    it('can logout', logout);

    it('can ldap login', login.bind(null, username, password));
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can check head content', checkHeadContent);
    it('can logout', logout);

    it('can ldap login', login.bind(null, username, password));
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can check head content', checkHeadContent);
    it('can logout', logout);

    it('can ldap login', login.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // no sso
    it('install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can set head content', setHeadContent);
    it('can check head content', checkHeadContent);
    it('can logout', logout);
    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app (for update)', function () { execSync('cloudron install --appstore-id app.xbackbone.cloudronapp --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can set head content', setHeadContent);
    it('can check head content', checkHeadContent);
    it('can logout', logout);

    it('can update', function () {
        execSync('cloudron update --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('can admin login', login.bind(null, 'admin', 'changeme'));
    it('can check head content', checkHeadContent);
    it('can logout', logout);

    it('can ldap login', login.bind(null, username, password));
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
